# humble

## 启动容器
```shell
docker compose up -d
```

## 进入容器
```shell
docker exec -it humble-core-1 bash
```

## 停止容器
```shell
docker stop humble-core-1
```
