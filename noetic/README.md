# noetic

## 启动容器
```shell
docker compose up -d
```

## 进入容器
```shell
docker exec -it noetic-core-1 bash
```

## 停止容器
```shell
docker stop noetic-core-1
```